# Automatise la connexion ssh + openconnect avec WSL

## Prérequis

1- WSL<br />
2- Accès ssh à WSL.<br />
3- Ajout de "AllowTcpForwarding yes" dans sshd_config de WSL.<br />
4- Installation de openconnect.<br />
```
sudo apt install openconnect
```
5- Script [csd-post.sh](https://gitlab.com/openconnect/openconnect/-/blob/master/trojans/csd-post.sh) dans ~/openconnect/<br />
```
mkdir ~/openconnect
wget https://gitlab.com/openconnect/openconnect/-/blob/master/trojans/csd-post.sh -o ~/openconnect/csd-post.sh

```

## Créer un nouveau raccourci pour lancer le script :

emplacement de pwsh.exe (ou powershell.exe)  -executionpolicy ByPass -File chemin du script
ex:<br />
**"C:\Program Files\PowerShell\7\pwsh.exe" -executionpolicy ByPass -File "C:\Users\jp\Desktop\televpn\vpnTéléTravail.ps1"**<br />

Lors de la première exécution le fichier de conf est généré dans le même dossier que le script, il faut l'éditer.<br />
Vérifier l'emplacement du script "csd-post.sh".<br />
## Pour ne pas saisir à chaque fois le mot de passe sudo pour openconnect<br />
```
# sudo visudo
# et ajouter en fin de fichier :
# nom_user ALL=(ALL) NOPASSWD: /usr/sbin/openconnect
# ex: john ALL=(ALL) NOPASSWD: /usr/sbin/openconnect
```
## Le fichier "télétravail.ppx"
fichier de conf de proxifier (créé un profil télétravail dans proxifier).<br />
Ajouter ceraines IP / domaines à mettre en accès direct : url du vpn, ip amon pam, ip du wsl...

