$scriptPath = $PSScriptRoot
$conf = Join-Path $scriptPath "conf.ps1"

if (!(Test-Path $conf -PathType Leaf)) {
	Write-Host "Fichier de conf absent !" -ForegroundColor red
	Write-Host "Editer et configurez conf.ps1, dans le dossier du script" -ForegroundColor red
    $scriptContent = @"
`$port = 9524 # port pour le socks
`$userSSH = "userssh"
`$passSSH = "password"
`$userVPN = "uservpn"
`$vpnURL = "url du vpn"
`$cmdOpenconnect = "sudo openconnect --user=`$userVPN --os=win --useragent='AnyConnect Windows 4.10.06079' --csd-wrapper ~/openconnect/csd-post.sh `$vpnURL -b --passwd-on-stdin"
`$urlToTestVpn = "192.168.23.81"
`$urlToTest = "https://www.google.fr"
"@
	$scriptContent | Out-File -FilePath $conf -Force
	Write-Host "Appuyez sur une touche pour continuer..."
	$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyUp')
	exit
} 

. $conf

if ((Get-Command "curl.exe" -ErrorAction SilentlyContinue) -eq $null) {
	Write-Host "curl.exe n'est pas présent dans le PATH" -ForegroundColor red
	start-sleep -Seconds 5
	exit
}
function testSocks {
	# on test l'accès à google via le proxy avec curl
	$testProxy = (curl -x socks5h://127.0.0.1:$port -s $urlToTest | Out-Null)
	if ($LASTEXITCODE -eq 0 ) {
		return $true
	}
	return $false
}

function testTunnel {
	# On pingue un IP uniquement accessible via le tunnel 
	$testTunnel = (wsl ping -c 1 $urlToTestVpn 2>&1)
	if ($testTunnel -like "*Network is unreachable*") {
		# Soucis avec wsl quand on coupe le tunnel (ex ctrl+c), où ça pète le réseau, il faut redémarrer wsl
		Write-Host "Le réseau est inaccessible, redémarrage de wsl" -ForegroundColor red
		wsl --shutdown
		start-sleep 5
		return $false
	}
	if (!($testTunnel -like "*errors*")) {
		return $true
	}
	return $false
}
function checkOpenconnectProcess{
	$psOpenconnect = (wsl bash -c "ps aux | grep -w openconnect | grep -v grep | awk '{print \`$11}'")
	if ($psOpenconnect -like "*openconnect*"){
		return $true
	}
	return $false
}
function getIP {
	# Récupération de l'ip de wsl 
	$ip = (wsl bash -c "ip a | grep inet | grep eth0 | cut -d'/' -f 1 | awk '{print \`$2}'")
	return $ip
}

function testAccessWSL {
	# on teste la disponibilité de wsl, en testant la version d'openSSH, quand ça répond c'est que wsl est accessible et le ssh aussi
	$sshAvailable = (wsl ssh -V 2>&1)
	if ($sshAvailable -like "OpenSSH*") {
		return $true
	}
	return $false
}
# nécessite le module WriteAscii
# Install-Module WriteAscii
if (Get-Module -ListAvailable -Name WriteAscii){
	Write-Ascii  -InputObject "hide from pat V1" -ForegroundColor DarkYellow
}

Write-Host " "
While ($true) {
	# On teste le socks
	Write-Host "Test du proxy socks sur le port $port : " -nonewline
	if (testSocks) {
		Write-Host -ForegroundColor green "OK"
		Write-Host "Proxy socks opérationnel." -ForegroundColor green
		#Si le socks est opérationnel, on peut quitter
		Write-Host "Appuyez sur une touche pour continuer..."
		$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyUp')
		exit
	} else {
		Write-Host -ForegroundColor red "Non opérationnel"
	}

	Write-Host "Attente de l'accès a wsl : " -nonewline
	
	while ($true) {
		# Vérifier si SSH est disponible dans WSL
		if (testAccessWSL) {
			Write-Host "OK" -ForegroundColor green
			break
		}
		start-sleep -Seconds 2
	}
	
	$ip = getIP
	if (!(testTunnel) -Or !(checkOpenconnectProcess)) {
		$pwd_string = Read-Host "Saisir le code PIN" -MaskInput
		start-process -filePath "wsl.exe" -ArgumentList "echo $pwd_string | $cmdOpenconnect"
	}
	
	Write-Host "Attente de ping à travers le VPN...  " -nonewline
	while ($true) {
		if (testTunnel) {
			$idOpenconnect = (wsl bash -c "ps aux | grep -w openconnect | grep -v grep | awk '{print \`$2}'")
			Write-Host ": OK " -nonewline -ForegroundColor green
			Write-Host "(ID process : $idOpenconnect)"
			#Write-Host "Lancement de putty vers $ip, port dynamique: $port." -ForegroundColor blue
			#Write-Host "Saisir le mot de passe SSH de wsl, si pageant n'est pas lancé." -ForegroundColor red
			start-process -filePath "putty.exe" -ArgumentList "-D $port -C -l $userSSH -pw $passSSH $ip" -WindowStyle Minimized
		break
		}
		Start-Sleep -Seconds 2
	}
}